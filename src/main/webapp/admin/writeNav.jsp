<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>写导航页面</title>
<link rel="stylesheet" type="text/css" href="${ctx}/static/jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${ctx}/static/jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript" src="${ctx}/static/jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx}/static/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>

<script type="text/javascript" charset="gbk" src="${ctx}/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="gbk" src="${ctx}/static/ueditor/ueditor.all.min.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="gbk" src="${ctx}/static/ueditor/lang/zh-cn/zh-cn.js"></script>

<script type="text/javascript">
	
	function submitData(){
		var navName=$("#navName").val();
		var navPageCode=$("#navPageCode").val();
		var navColor=$("#navColor").val();
		var navIcon=$("#navIcon").val();
		var orderNo=$("#orderNo").val();
		var content=UE.getEditor('editor').getContent()
		
		if(navName==null || navName==''){
			alert("请输入导航标题！");
		}else if(navPageCode==null || navPageCode==''){
			alert("请输入导航链接标识！");
		}else if(content==null || content==''){
			alert("请填写内容！");
		}else{
			$.post("${ctx}/admin/nav/save.do",{'navName':navName,'navPageCode':navPageCode,
				'navColor':navColor,'navIcon':navIcon,'orderNo':orderNo,'content':content},function(result){
				if(result.success){
					alert("导航页面保存成功！");
					resultValue();
				}else{
					alert("保存失败！");
				}
			},"json");
		}
	}
	
	function resultValue(){
		$("#navName").val("");
		$("#navPageCode").val("");
		$("#navIcon").val("");
		$("#navColor").val("");
		$("#orderNo").val("");
		UE.getEditor('editor').setContent("");
	}
</script>
</head>
<body style="margin: 10px">

<div id="p" class="easyui-panel" title="修改导航" style="padding: 10px">
	<table cellspacing="20px">
		<tr>
			<td width="80px">导航标题：</td>
			<td>
				<input type="text" id="navName" name="navName" style="width: 400px"/>
			</td>
		</tr>
		<tr>
			<td width="80px">导航标识：</td>
			<td>
				<input type="text" id="navPageCode" name="navPageCode" style="width: 200px"/><font color="red">英文标识链接符</font>
			</td>
		</tr>
		<tr>
			<td width="80px">导航颜色：</td>
			<td>
				<input type="text" id="navColor" name="navColor" style="width: 200px"/>
			</td>
		</tr>
		<tr>
			<td width="80px">排序顺序：</td>
			<td>
				<input type="text" id="orderNo" name="orderNo" style="width: 200px"/><font color="red">排序在前的在前面，1最前</font>
			</td>
		</tr>
		<tr>
			<td valign="top">导航页面内容：</td>
			<td>
				<script id="editor" name="content" type="text/plain" style="width:100%;height:500px;"></script>
			</td>
		</tr>		
		<tr>
			<td></td>
			<td>
				<a href="javascript:submitData()" class="easyui-linkbutton" data-options="iconCls:'icon-submit'">修改页面</a>
			</td>
		</tr>
	</table>
</div>

<!-- 实例化编辑器 -->
<script type="text/javascript">
    var ue = UE.getEditor('editor');
    
</script>



</body>
</html>