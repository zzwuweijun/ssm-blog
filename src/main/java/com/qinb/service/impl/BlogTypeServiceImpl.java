package com.qinb.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.qinb.dao.BlogTypeDao;
import com.qinb.entity.BlogType;
import com.qinb.service.BlogTypeService;

/**
 * 博客类型Service实现类
 * @author Administrator
 *
 */
@Service("blogTypeService")
public class BlogTypeServiceImpl implements BlogTypeService{

	@Resource
	private BlogTypeDao blogTypeDao;
	
	public List<BlogType> countList() {
		return blogTypeDao.countList();
	}

	@Override
	public BlogType findById(Integer id) {
		// TODO Auto-generated method stub
		return blogTypeDao.findById(id);
	}

	@Override
	public List<BlogType> list(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return blogTypeDao.list(map);
	}

	@Override
	public Long getTotal(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return blogTypeDao.getTotal(map);
	}

	@Override
	public Integer add(BlogType blogType) {
		// TODO Auto-generated method stub
		return blogTypeDao.add(blogType);
	}

	@Override
	public Integer update(BlogType blogType) {
		// TODO Auto-generated method stub
		return blogTypeDao.update(blogType);
	}

	@Override
	public Integer delete(Integer id) {
		// TODO Auto-generated method stub
		return blogTypeDao.delete(id);
	}

	
}
