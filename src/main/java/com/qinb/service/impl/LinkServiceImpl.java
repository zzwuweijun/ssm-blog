package com.qinb.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.qinb.dao.LinkDao;
import com.qinb.entity.Link;
import com.qinb.service.LinkService;

/**
 * 友情链接Service实现类
 * @author Administrator
 *
 */
@Service("linkService")
public class LinkServiceImpl implements LinkService{

	@Resource
	private LinkDao linkDao;

	public List<Link> list(Map<String, Object> map) {
		return linkDao.list(map);
	}

	@Override
	public Long getTotal(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return linkDao.getTotal(map);
	}

	@Override
	public Integer add(Link link) {
		// TODO Auto-generated method stub
		return linkDao.add(link);
	}

	@Override
	public Integer update(Link link) {
		// TODO Auto-generated method stub
		return linkDao.update(link);
	}

	@Override
	public Integer delete(Integer id) {
		// TODO Auto-generated method stub
		return linkDao.delete(id);
	}

	
	
	

}
