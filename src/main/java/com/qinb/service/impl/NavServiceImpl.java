package com.qinb.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.qinb.dao.NavDao;
import com.qinb.entity.Nav;
import com.qinb.service.NavService;

@Service("navService")
public class NavServiceImpl implements NavService {

	@Resource
	private NavDao navDao;
	
	@Override
	public List<Nav> list(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return navDao.list(map);
	}

	@Override
	public Long getTotal(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return navDao.getTotal(map);
	}

	@Override
	public Nav findById(Integer id) {
		// TODO Auto-generated method stub
		return navDao.findById(id);
	}

	@Override
	public Integer add(Nav nav) {
		// TODO Auto-generated method stub
		return navDao.add(nav);
	}

	@Override
	public Integer update(Nav nav) {
		// TODO Auto-generated method stub
		return navDao.update(nav);
	}

	@Override
	public Integer delete(Integer id) {
		// TODO Auto-generated method stub
		return navDao.delete(id);
	}

	@Override
	public Nav findByPageCode(String navPageCode) {
		// TODO Auto-generated method stub
		return navDao.findByPageCode(navPageCode);
	}

}
