package com.qinb.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.qinb.entity.Nav;
import com.qinb.service.NavService;


@Controller
@RequestMapping("/")
public class NavController {

	@Resource
	private NavService navService;
	
	@RequestMapping("/{navPageCode}")
	public ModelAndView redirect(@PathVariable("navPageCode") String navPageCode,HttpServletRequest request)throws Exception{
		ModelAndView mav = new ModelAndView();
		Nav nav = navService.findByPageCode(navPageCode);
		mav.addObject("nav", nav);
		mav.addObject("pageTitle", nav.getNavName());
		mav.addObject("mainPage","foreground/system/navContent.jsp");
		mav.setViewName("mainTemp");
		return mav;
	}
	
}
