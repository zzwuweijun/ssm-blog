package com.qinb.dao;

import java.util.List;
import java.util.Map;

import com.qinb.entity.Nav;

public interface NavDao {

	public List<Nav> list(Map<String,Object> map);
	
	public Long getTotal(Map<String,Object> map);
	
	public Nav findById(Integer id);
	
	public Nav findByPageCode(String navPageCode);
	
	public Integer add(Nav nav);
	
	public Integer update(Nav nav);
	
	public Integer delete(Integer id);
}
