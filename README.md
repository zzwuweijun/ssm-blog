# SSM实用博客系统

#### 项目介绍
SSM实用博客系统

#### 软件架构
软件架构说明
SpringMVC,Spring,Mybatis,Lucene,Shiro

#### 安装教程

数据库文件在resource/doc下面db_blog.sql<br>
1. 先导入数据库文件，修改applicationContext.xml中的数据库连接信息。<br>
2. 修改tomcat部署方式，将path修改为/

```
<Context docBase="Blog" path="/" reloadable="true" source="org.eclipse.jst.jee.server:Blog"/></Host>
```

3. 部署访问http://localhost:8080

4. 后台登陆地址:http://localhost:8080/login.jsp
初始化账号密码：admin/123456

#### 运行截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1216/191615_4a0e2a3e_732467.png "TIM截图20181216190319.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1216/191632_4908ef0c_732467.png "TIM截图20181216191335.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1216/191645_01bdfb18_732467.png "TIM截图20181216190719.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1216/191658_b904d705_732467.png "TIM截图20181216190730.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1216/191714_191e6b0c_732467.png "TIM截图20181216190845.png")


#### 使用说明

1. 搜索功能只能在项目运行后新添加的博客才可以搜索到，因为添加博客的时候会添加lucene索引。
2. 菜单导航都是可以自定义的，具体看后台的导航管理
3. 如果后台出现页面打不开的，样式错乱，可能是tomcat没有配置访问路径，需要在tomcat中将path改为/.
4. ...

其他项目 springboot+mybatis的博客系统：https://gitee.com/imqinbao/javabb_blog
在不懂的问我：QQ 904274014 Java技术交流群：812423757






